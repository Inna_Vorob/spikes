﻿using UnityEngine;
using System.Collections;

public class EnemyController
{
    public Transform EnemyTransform { get; private set; }
    public string TypeOfWall { get; private set; }  
    
    private GameObject _enemy; 
    private SpriteRenderer _enemySprite; 
        
    private int _direction;
    private int _startDirection;
    private float _stepX; 

    public EnemyController(GameObject enemy,string typeOfWall)
    {
        _enemy = enemy;
        EnemyTransform = _enemy.GetComponent<Transform>();
        _enemySprite = _enemy.GetComponent<SpriteRenderer>();
        _stepX = _enemySprite.bounds.size.x / 2f;
        TypeOfWall = typeOfWall;

        SetDirection(TypeOfWall);
        _startDirection = _direction;
    }

    private void SetDirection(string typeOfWall)
    {
        switch (typeOfWall)
        {
            case "Left":
                _direction = 1;
                break;

            case "Right":
                _direction = -1;
                break;
        }
    }

    public bool IsActive()
    {
        return _enemy.activeInHierarchy;
    }

    public void SetActiveYesNo(bool set)
    {
        _enemy.SetActive(set);
    }
      
    public IEnumerator MoveEnemy(bool IsNeedDisactivate)
    {
        float startPos = EnemyTransform.position.x;
        float finishPos = startPos + _direction * _stepX;
        float speed = 0.3f * Time.deltaTime;

        while ((EnemyTransform.position.x < finishPos && _direction == 1) ||
                (EnemyTransform.position.x > finishPos && _direction == -1))
        {
             EnemyTransform.position = new Vector3(EnemyTransform.position.x + _direction * speed, EnemyTransform.position.y, EnemyTransform.position.z);
            
            yield return null;
        }
      
        if (IsNeedDisactivate)
            SetActiveYesNo(false);

        yield break;
    }

    public void ResetDirection()
    {
        _direction *= -1;
    }

    public void ResetDirectionToStart()
    {
        _direction = _startDirection;
    }
}
