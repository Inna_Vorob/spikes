﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class TouchController : MonoBehaviour, IPointerClickHandler
{
    public delegate void OnClickEvent();
    public static event OnClickEvent OnClick;
    
    public void OnPointerClick(PointerEventData eventData)
    {
        if (OnClick != null)
            OnClick();
    }  
}
