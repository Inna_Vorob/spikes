﻿using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour {
       
    private EnemyController _enemyOne;     

    private GameObject _enemy;
    public GameObject Enemy
    {
        get
        {
            if (_enemy == null)
            {
                { _enemy = Resources.Load<GameObject>("Prefabs/Enemy");
                    _enemy.transform.localScale =new Vector2( VersionController.ScaleOfObject, VersionController.ScaleOfObject);
                }

                if (_enemy == null)
                    Debug.LogError("Can not load Prefabs/Enemy");
            }
            return _enemy;
        }
    }

    private SpriteRenderer _enemySprite;
    public SpriteRenderer EnemySprite
    {
        get
        {
            if (_enemySprite == null) // convert to '??' expression
                _enemySprite = Enemy.GetComponent<SpriteRenderer>();

            return _enemySprite;
        }
    }

    private List<EnemyController> _poolEnemysList;
    public List<EnemyController> PoolEnemysList
    {
        get
        {
            if (_poolEnemysList == null)
                _poolEnemysList = new List<EnemyController>();

            return _poolEnemysList;
        }
    }    

    public float StepX { get { return EnemySprite.bounds.size.x / 2f ; } }

    #region Init Singleton
    private static SpawnEnemy _instance;
    public static SpawnEnemy Instance
    {
        get
        {
            if (_instance == null) 
            {
                _instance = FindObjectOfType<SpawnEnemy>();
            }
            return _instance;
        }
    }
    private SpawnEnemy()
    {
    }
    #endregion
       
    public void AddEnemy(Vector3 positionOfEnemy,string typeOfWall)
    {
        bool isAddEnemy = false;
        positionOfEnemy.x = positionOfEnemy.x + InitStep(typeOfWall);

        for (int i = 0; i < PoolEnemysList.Count; i++)
        {
            if (!PoolEnemysList[i].IsActive())
            {
                PoolEnemysList[i].EnemyTransform.position = positionOfEnemy;
                PoolEnemysList[i].SetActiveYesNo(true);

                if (typeOfWall == "Left" || typeOfWall == "Right")
                {
                    PoolEnemysList[i].ResetDirectionToStart();
                    StartCoroutine(PoolEnemysList[i].MoveEnemy(false));
                }               
                isAddEnemy = true;
                break;
            }
        }

        if (!isAddEnemy)
        {  
            GameObject objectEnemy = Instantiate(Enemy, Vector3.ClampMagnitude(positionOfEnemy,5), Enemy.transform.rotation) as GameObject;
            _enemyOne = new EnemyController(objectEnemy,typeOfWall);

            if (typeOfWall == "Left" || typeOfWall == "Right")
            {                      
                StartCoroutine(_enemyOne.MoveEnemy(false));               
            }
            PoolEnemysList.Add(_enemyOne);
        }
    }

    public float [] GetArrayOfPoints(int countOfPoint,float segment,float startPoint)
    {
        float step = segment / (countOfPoint + 1);

        float[] ArrayPoints = new float[countOfPoint];

        ArrayPoints[0] = startPoint + step;

        for (int i = 1; i < countOfPoint; i++)
        {
            ArrayPoints[i] = ArrayPoints[i - 1] + step;
        }
        return ArrayPoints;
    }


    public float [] ChooseRandomPositionFromArray(int numRequired, float [] array)
    {
        float[] result = new float[numRequired];

        int numToChoose = numRequired;

        for (int numLeft = array.Length; numLeft > 0; numLeft--)
        {

            float prob = (float)numToChoose / (float)numLeft;

            if (Random.value <= prob)
            {
                numToChoose--;
                result[numToChoose] = array[numLeft - 1];

                if (numToChoose == 0)
                {
                    break;
                }
            }
        }
        return result;
    }

    public void DisactivateWall(string typeOfWall)
    {       
        for (int i = 0; i < PoolEnemysList.Count; i++)
        {
            if(PoolEnemysList[i].TypeOfWall == typeOfWall)
            {             
                PoolEnemysList[i].ResetDirection();
                StartCoroutine(PoolEnemysList[i].MoveEnemy(true));               
            }                 
        }
    }

    private float InitStep(string typeOfWall)
    {
        float step = 0;

        switch (typeOfWall)
        {
            case "Left":
               step = -StepX;
                break;

            case "Right":
               step = StepX;
                break;        
        }
        return step;

    }
    
     
}



