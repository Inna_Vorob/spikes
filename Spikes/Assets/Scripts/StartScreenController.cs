﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class StartScreenController : MonoBehaviour , IPointerClickHandler
{
    private const int HIDE_SCREEN = -5;
    private const int SHOW_SCREEN = 0;

    private Canvas _screen;    
    private Text _textBestScore;
    private Text _textGamesPlayed;
    private Text _textCountOfCandy;
    private Image _buttonSound;

    private string _soundState;
    private Color _onColor;
    private Color32 _offColor;

    private AudioSource _audioTap;

    void Awake()
    {
        _soundState = PlayerPrefsController.Instance.SoundState;
        _audioTap = GameObject.Find("CanvasStartScreen").GetComponent<AudioSource>();
        _screen = GameObject.Find("CanvasStartScreen").GetComponent<Canvas>();        
        _textBestScore = GameObject.Find("BestScoreStart").GetComponent<Text>();
        _textGamesPlayed = GameObject.Find("GamesPlayedStart").GetComponent<Text>();
        _textCountOfCandy = GameObject.Find("CandyCount").GetComponent<Text>();
        _buttonSound = GameObject.Find("AudioButton").GetComponent<Image>();

        _onColor = _buttonSound.color;
        _offColor = new Color32(180,176,176,255);

        if (_soundState == "Off")
        {
            _buttonSound.color = _offColor;      
        }
        if (_soundState == "On")
        {
            _buttonSound.color = _onColor;      
        }
    }
    
    public void OnPointerClick(PointerEventData eventData)
    {
        GameController.Instance.SetGameState(STATE.GAME_SCREEN);
    }

    public void StartScreenStart()
    {
        _screen.sortingOrder = SHOW_SCREEN;       
        _textBestScore.text = "BEST SCORE : " + PlayerPrefsController.Instance.BestScore.ToString();
        _textGamesPlayed.text = "GAMES PLAYED : " + PlayerPrefsController.Instance.GamesPlayed.ToString();
        _textCountOfCandy.text = PlayerPrefsController.Instance.CandyCount.ToString();
    }

    public void StartScreenEnd()
    {       
        _screen.sortingOrder = HIDE_SCREEN;      
        _textBestScore.text = "";
        _textGamesPlayed.text = "";
        _textCountOfCandy.text = "";
    }

    public void Menu()
    {
        PlaySound();
        GameController.Instance.SetGameState(STATE.MAGAZINE_SCREEN);
    }

    public void ClickSound()
    {
        if (_soundState == "On")
        {
            _soundState = "Off";
            _buttonSound.color = _offColor;
            PlayerPrefsController.Instance.SaveSoundStateInformation(_soundState);
        }
        else
        {
            if (_soundState == "Off")
            {
                _soundState = "On";
                PlayerPrefsController.Instance.SaveSoundStateInformation(_soundState);
                _buttonSound.color = _onColor;
                PlaySound();
            }
        }
    }

    private void PlaySound()
    {
        if (PlayerPrefsController.Instance.SoundState == "On")
        {
            _audioTap.Play();
        }
    }
}
