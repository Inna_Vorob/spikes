﻿using UnityEngine;
using System.Collections;

 public struct CandyCountController
{   
    public int Count { get; set; }
    public CandyCountController(int count)
    {
        Count = count;
    }
    public void ChangeCount()
    {
        ++Count;    
    }  
}
