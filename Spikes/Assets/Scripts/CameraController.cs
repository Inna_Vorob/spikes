﻿using UnityEngine;

public static class CameraController {

    private static Camera _mainCamera;
    public static Camera mainCamera
    {
        get
        {
            if (_mainCamera == null) 
                _mainCamera = Camera.main;
            return _mainCamera;
        }
    } 

    public static float OrthographicSize
    {
        get
        {
            if (!mainCamera.orthographic)
                mainCamera.orthographic = true;

            return mainCamera.orthographicSize;
        }
    }

    public static float Aspect { get { return mainCamera.aspect; } }   
    public static float RightSideCamera { get { return OrthographicSize * Aspect; } }   
    public static float LeftSideCamera { get { return  -OrthographicSize * Aspect; } }
    public static float TopSideCamera { get { return OrthographicSize; } }
    public static float BottomSideCamera { get { return -OrthographicSize; } }
    public static float HeightCamera { get { return 2 * OrthographicSize; } }
    public static float WidthCamera { get { return HeightCamera *Aspect; } }
}
