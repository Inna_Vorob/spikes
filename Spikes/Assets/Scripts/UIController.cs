﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    private const int HIDE_SCREEN = -5;
    private const int SHOW_SCREEN = -1;

    private const int COUNT_ENEMYS_TOP_BOTTOM = 5;
    private const int COUNT_ENEMYS_LEFT_RIGHT = 7;

    private Text _scoreText;

    public float StepWidth { get; private set; } 
    public float StepHeight { get; private set; }    
    public float LeftWall { get; private set; }
    public float RightWall { get; private set; }
    public float TopWall { get; private set; }
    public float BottomWall { get; private set; }

    private RectTransform _leftWallRT;
    private RectTransform _rightWallRT;
    private RectTransform _topWallRT;
    private RectTransform _bottomWallRT;

    private Image _backGround;
    private Color _startColorBackGround;
    
    private Canvas _circleScreen;

    private BoxCollider2D _wallLRCollider;
    private Transform _wallLRTransform;

    private BoxCollider2D _wallTBCollider;
    private Transform _wallTBTransform;

    private GameObject _wallLR;
    public GameObject WallLeftRight
    {
        get
        {
            if (_wallLR == null)
            {
                _wallLR = Resources.Load<GameObject>("Prefabs/WallLeftRight");

                if (_wallLR == null)
                    Debug.LogError("Can not load Prefabs/WallLeftRight");
            }
            return _wallLR;
        }
    }

    private GameObject _wallTB;
    public GameObject WallTopBottom
    {
        get
        {
            if (_wallTB == null)
            {
                _wallTB = Resources.Load<GameObject>("Prefabs/WallTopBottom");

                if (_wallTB == null)
                    Debug.LogError("Can not load Prefabs/WallTopBottom");
            }
            return _wallTB;
        }
    }

    void Awake()
    {
        _scoreText = gameObject.GetComponentInChildren<Text>();

        _leftWallRT = GameObject.FindGameObjectWithTag("LeftWall").GetComponent<RectTransform>();
        _rightWallRT = GameObject.FindGameObjectWithTag("RightWall").GetComponent<RectTransform>();
        _topWallRT = GameObject.FindGameObjectWithTag("TopWall").GetComponent<RectTransform>();
        _bottomWallRT = GameObject.FindGameObjectWithTag("BottomWall").GetComponent<RectTransform>();       
        
        _circleScreen = GameObject.Find("CanvasGameScore").GetComponent<Canvas>();
        _backGround = GameObject.Find("BackGroundGame").GetComponent<Image>();
        _startColorBackGround = _backGround.color;
        

        InitStep();
        SpawnTopBottomSpikes();
        InitWalls();
      
      
    }
   
    void OnEnable()
    {
        ScoreController.OnChange += ChangeText;
        ScoreController.OnChange += SpawnEnemys;
        ScoreController.OnChange += ChangeColor;
    }

    void OnDisable()
    {
        ScoreController.OnChange -= ChangeText;
        ScoreController.OnChange -= SpawnEnemys;
        ScoreController.OnChange -= ChangeColor;
    }

    private void ChangeText(int count)
    {
        _scoreText.text = count.ToString();
    }

    private void SpawnEnemys(int count)
    {
        if (!IsEvenNumber(count))
        {
            SpawnSpikesRandom(GetCountOfEnemys(count), LeftWall, "Left");
            SpawnEnemy.Instance.DisactivateWall("Right");
        }

        else
        {
            SpawnSpikesRandom(GetCountOfEnemys(count), RightWall, "Right");
            SpawnEnemy.Instance.DisactivateWall("Left");
        }         
        
    }

    private void ChangeColor(int count)
    { 
        if (IsFiveFoldNumber(count))
        {
            Color c = _backGround.color;
            float value = (c.r + c.g + c.b) / 3;
            float newValue = value + 2f * 0.45f * 0.2f - 0.2f;
            float valueRatio = newValue / value;
            Color newColor = new Color();
            newColor.r = c.r * valueRatio;
            newColor.g = c.g * valueRatio;
            newColor.b = c.b * valueRatio/0.9f;
            newColor.a = 1;
            _backGround.color = newColor;
        }
    }
    

    private bool IsEvenNumber(int count)
    {
        return count % 2 == 0;
    }

    private bool IsFiveFoldNumber(int count)
    {
        return count % 5 == 0;
    }

    private int GetCountOfEnemys(int count)
    {
        int countOfEnemys = 0;

        if (count == 1)
            countOfEnemys = 1;
        if (count > 1 && count <= 4)
            countOfEnemys = 2;
        if (count > 4 && count <= 9)
            countOfEnemys = 3;
        if (count > 9 && count <= 19)
            countOfEnemys = Random.Range(3, 5);
        if (count > 19 && count <= 59)
            countOfEnemys = Random.Range(4, 6);
        if (count > 59)
            countOfEnemys = Random.Range(5, 7);

        return countOfEnemys;
    }

    private void InitStep()
    {
        LeftWall = CameraController.LeftSideCamera + CameraController.WidthCamera * _leftWallRT.anchorMax.x;
        RightWall = CameraController.LeftSideCamera + CameraController.WidthCamera * _rightWallRT.anchorMin.x;
        TopWall = CameraController.BottomSideCamera + CameraController.HeightCamera * _topWallRT.anchorMin.y;
        BottomWall = CameraController.BottomSideCamera + CameraController.HeightCamera * _bottomWallRT.anchorMax.y;
        
        StepWidth = RightWall - LeftWall;
        StepHeight = TopWall - BottomWall;
       
    }

    private void SpawnTopBottomSpikes()
    {
        float[] arrayTopBottom = SpawnEnemy.Instance.GetArrayOfPoints(COUNT_ENEMYS_TOP_BOTTOM, StepWidth, LeftWall);      

        for (int i = 0; i < arrayTopBottom.Length; i++)
        {           
            SpawnEnemy.Instance.AddEnemy(new Vector3(arrayTopBottom[i], TopWall, 0f),"Top");
            SpawnEnemy.Instance.AddEnemy(new Vector3(arrayTopBottom[i], BottomWall, 0f),"Bottom");

        }
    }

    private void SpawnSpikesRandom(int countSpikes,float wall,string nameWall)
    {
        float[] array = SpawnEnemy.Instance.GetArrayOfPoints(COUNT_ENEMYS_LEFT_RIGHT, StepHeight, BottomWall);
        float[] newArray = SpawnEnemy.Instance.ChooseRandomPositionFromArray(countSpikes, array);

        for (int i = 0; i < newArray.Length; i++)
        {
            SpawnEnemy.Instance.AddEnemy(new Vector3(wall, newArray[i], 0f), nameWall);
        }

    }

    public void HideScorePanel()
    {
        _scoreText.text = "";
        _circleScreen.sortingOrder = HIDE_SCREEN;
    }

    public void ShowScorePanel()
    {
        _circleScreen.sortingOrder = SHOW_SCREEN;
    }

    public void HideLeftRightSpikes()
    {
        SpawnEnemy.Instance.DisactivateWall("Right");
        SpawnEnemy.Instance.DisactivateWall("Left");
    }

    private void InitWalls()
    {
        GameObject objectEnemy;

        _wallLRCollider = WallLeftRight.GetComponent<BoxCollider2D>();
        _wallLRTransform = WallLeftRight.GetComponent<Transform>();

        _wallTBCollider = WallTopBottom.GetComponent<BoxCollider2D>();
        _wallTBTransform = WallTopBottom.GetComponent<Transform>();

        _wallLRCollider.size = new Vector2(0.01f, StepHeight);
        _wallTBCollider.size = new Vector2(StepWidth, 0.01f);

        //Left      
        _wallLRTransform.position = new Vector2(LeftWall, 0f);
        objectEnemy = Instantiate(WallLeftRight, _wallLRTransform.position, Quaternion.identity) as GameObject;

        //Right       
        _wallLRTransform.position = new Vector2(RightWall, 0f);
        objectEnemy = Instantiate(WallLeftRight, _wallLRTransform.position, Quaternion.identity) as GameObject;

        //Top      
        _wallTBTransform.position = new Vector2(0f, TopWall);
        objectEnemy = Instantiate(WallTopBottom, _wallTBTransform.position, Quaternion.identity) as GameObject;

        //Bottom      
        _wallTBTransform.position = new Vector2(0f, BottomWall);
        objectEnemy = Instantiate(WallTopBottom, _wallTBTransform.position, Quaternion.identity) as GameObject;
    } 

    public void SetStartColor()
    {
        _backGround.color = _startColorBackGround;
    }

   

}
