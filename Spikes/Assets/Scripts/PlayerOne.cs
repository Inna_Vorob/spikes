﻿using UnityEngine;
using System.Collections;

public class PlayerOne {

    public Transform PlayerTransform { get;  set; }
    private GameObject _player;

    public int numberOfPlayer;

    public Rigidbody2D playerRigidbody;
    public SpriteRenderer playerSprite;
    public ParticleSystem playerParticleSystem;

    public Vector3 startPosition;
    public Color startColor;

    public PlayerOne(GameObject player, int number)
    {
        _player = player;
        numberOfPlayer = number;
        
        PlayerTransform = _player.GetComponent<Transform>();
        PlayerTransform.localScale = new Vector2(VersionController.ScaleOfObject, VersionController.ScaleOfObject);
        playerRigidbody = _player.GetComponent<Rigidbody2D>();
        playerSprite = _player.GetComponent<SpriteRenderer>();
        playerParticleSystem = _player.GetComponentInChildren<ParticleSystem>();

        startPosition = PlayerTransform.position;
        startColor = playerSprite.color;
    }

    public bool IsActive()
    {
        return _player.activeInHierarchy;
    }

    public void SetActiveYesNo(bool set)
    {
        _player.SetActive(set);
    }
}
