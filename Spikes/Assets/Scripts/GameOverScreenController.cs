﻿using UnityEngine;
using UnityEngine.UI;

public class GameOverScreenController : MonoBehaviour
{
    private const int HIDE_SCREEN = -5;
    private const int SHOW_SCREEN = 0;

    private Canvas _screen;
    private Text _textScore;
    private Text _textBestScore;
    private Text _textGamesPlayed;
    private Text _textCountOfCandy;

    private AudioSource _audioButton;   
   
    private PlayerController _player;   

    void Awake()
    {
        _screen = GameObject.Find("CanvasGameOver").GetComponent<Canvas>();
        _textScore = GameObject.Find("ScoreShow").GetComponent<Text>();
        _textBestScore = GameObject.Find("BestScore").GetComponent<Text>();
        _textGamesPlayed = GameObject.Find("GamesPlayed").GetComponent<Text>();
        _textCountOfCandy = GameObject.Find("CandyCountEnd").GetComponent<Text>();

        _player = GameObject.FindObjectOfType<PlayerController>();
        _audioButton = GameObject.Find("CanvasGameOver").GetComponent<AudioSource>();

    }  
    
    public void GameOverStart()
    {
        _screen.sortingOrder = SHOW_SCREEN;
        _textScore.text = _player.GetScoreOfThisPlayer().ToString();
        _textBestScore.text = "BEST SCORE : " + PlayerPrefsController.Instance.BestScore.ToString();
        _textGamesPlayed.text = "GAMES PLAYED : " + PlayerPrefsController.Instance.GamesPlayed.ToString();
        _textCountOfCandy.text = PlayerPrefsController.Instance.CandyCount.ToString();
    }

    public void GameOverEnd()
    {
        _screen.sortingOrder = HIDE_SCREEN;
        _textScore.text = "";
        _textBestScore.text = "";
        _textGamesPlayed.text = "";
        _textCountOfCandy.text = "";
    }

    public void Replay()
    {
        PlaySound();
        GameController.Instance.SetGameState(STATE.START_SCREEN);        
    }

    public void Exit()
    {
        PlaySound();
        Application.Quit();        
    }

    private void PlaySound()
    {
        if (PlayerPrefsController.Instance.SoundState == "On")
        {           
            _audioButton.Play();
        }
    }

}
