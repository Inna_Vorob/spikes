﻿using UnityEngine;
using System.Collections;

public struct ScoreController
{
    public delegate void OnChangeScore(int count);
    public static event OnChangeScore OnChange;
 
    public int Score { get; private set; }
    
    public ScoreController(int score)
    {
        Score = score;
    } 

    public void ChangeScore()
    {
        if (OnChange != null)
            OnChange(++Score);         
    }
}
