﻿using UnityEngine;

public class GameController : MonoBehaviour {

    #region Init Singleton     
    private static GameController _instance;
    public static GameController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameController>();
            }
            return _instance;
        }
    }
    private GameController()
    {
    }
    #endregion

    private PlayerController _playerController;
    private StateController _stateController;
    private UIController _uiController;
    private GameOverScreenController _gameOverScreen;
    private StartScreenController _startScreen; 
    private SpawnCandy _spawnCandy;
    private StoreScreenController _storeScreen;



    public UIController UIController { get { return _uiController; } }
    public PlayerController PlayerController { get { return _playerController; } }
    public GameOverScreenController GameOverScreen { get { return _gameOverScreen; } }
    public StartScreenController StartScreen { get { return _startScreen; } } 
    public SpawnCandy SpawnCandy { get { return _spawnCandy; } }
    public StoreScreenController StoreScreen { get { return _storeScreen; } }

    void Start()
    {
        _uiController = GameObject.FindObjectOfType<UIController>();
        _playerController = GameObject.FindObjectOfType<PlayerController>();
        _gameOverScreen = GameObject.FindObjectOfType<GameOverScreenController>();
        _startScreen = GameObject.FindObjectOfType<StartScreenController>();    
        _spawnCandy = GameObject.FindObjectOfType<SpawnCandy>();
        _storeScreen = GameObject.FindObjectOfType<StoreScreenController>();
        _stateController = new StateController();    
    }

    public void SetGameState(STATE state)
    {
        _stateController.MoveNext(state);
    }

}
