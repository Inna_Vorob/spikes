﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TMScreenController : MonoBehaviour {

    private const int HIDE_SCREEN = -5;
   
    private Canvas _screen;   
    private Image _imageTM;
    private Image _imageThinkMobiles;

    void Awake()
    {
        PlayerPrefsController.Instance.LoadAllInformation();
        _screen = GameObject.Find("ThinkMobilesScreen").GetComponent<Canvas>();        
        _imageTM = GameObject.Find("TMImage").GetComponent<Image>();
        _imageThinkMobiles = GameObject.Find("ThinkMobilesImage").GetComponent<Image>();
        StartCoroutine(Dead());
    }

    private IEnumerator Dead()
    {

        for (float f = 1f; f >= 0; f -= 0.04f)
        { 
            Color c = Color.white;
            c.a = f;
            _imageTM.color = c;
            _imageThinkMobiles.color = c;
          
            yield return new WaitForSeconds(0.06f);
        }
        _screen.sortingOrder = HIDE_SCREEN;
         GameController.Instance.SetGameState(STATE.START_SCREEN);
       
      
         yield break;
    }
}
