﻿using UnityEngine;
using System.Collections;

public class PlayerPrefsController : MonoBehaviour
{
    #region Init Singleton
    private static PlayerPrefsController _instance;
    public static PlayerPrefsController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<PlayerPrefsController>();
            }
            return _instance;
        }
    }
    private PlayerPrefsController()
    {
    }
    #endregion

    public int BestScore { get; private set; }
    public int GamesPlayed { get; private set; }  
    public int CandyCount { get; private set; }

    public string SoundState  { get; private set; }
    public int PlayerState { get; private set; }

    public string Button1State { get; private set; }
    public string Button2State { get; private set; }
    public string Button3State { get; private set; }
    public string Button4State { get; private set; }
    public string Button5State { get; private set; }

    public  void LoadBestScoreGamesPlayedInformation()
    {
        if (PlayerPrefs.HasKey("BestScore") && PlayerPrefs.HasKey("GamesPlayed"))
        {
            BestScore = PlayerPrefs.GetInt("BestScore");
            GamesPlayed = PlayerPrefs.GetInt("GamesPlayed");          
        }
        else
        {
            BestScore = 0;
            GamesPlayed = 0;                 
        }
   
    }

    public void LoadCandyCountInformation()
    {
        if (PlayerPrefs.HasKey("CandyCount"))
           CandyCount = PlayerPrefs.GetInt("CandyCount");
        else       
           CandyCount = 0;       
    }

    public void LoadSoundStateInformation()
    {
        if (PlayerPrefs.HasKey("SoundState"))
            SoundState = PlayerPrefs.GetString("SoundState");        
        else
            SoundState = "On";        
    }

    public void LoadPlayerStateInformation()
    {
        if (PlayerPrefs.HasKey("PlayerState"))
            PlayerState = PlayerPrefs.GetInt("PlayerState");
        else
            PlayerState = 1;
    }

    public void LoadButtonStateInformation()
    {
        if (PlayerPrefs.HasKey("Button1State") && PlayerPrefs.HasKey("Button2State") && PlayerPrefs.HasKey("Button3State")
           && PlayerPrefs.HasKey("Button4State") && PlayerPrefs.HasKey("Button5State"))
        {
            Button1State = PlayerPrefs.GetString("Button1State");
            Button2State = PlayerPrefs.GetString("Button2State");
            Button3State = PlayerPrefs.GetString("Button3State");
            Button4State = PlayerPrefs.GetString("Button4State");
            Button5State = PlayerPrefs.GetString("Button5State");
        }
        else
        {
            Button1State = "SELECT";
            Button2State = "NOTBUY";
            Button3State = "NOTBUY";
            Button4State = "NOTBUY";
            Button5State = "NOTBUY";
        }
    }

    public void SaveBestScoreGamesPlayedInformation(int currentScore)
    {
        if (currentScore > BestScore)
        {
            BestScore = currentScore;
            PlayerPrefs.SetInt("BestScore", BestScore);
        }

        GamesPlayed++;
        PlayerPrefs.SetInt("GamesPlayed",GamesPlayed);       
    }

    public void SaveCandyCountInformation(int countCandy)
    {
        CandyCount = countCandy;
        PlayerPrefs.SetInt("CandyCount", CandyCount);
    }

    public void SaveSoundStateInformation(string soundState)
    {
        SoundState = soundState;
        PlayerPrefs.SetString("SoundState", SoundState);
    }

    public void SavePlayerStateInformation(int numberOfPlayer)
    {
        PlayerState = numberOfPlayer;
        PlayerPrefs.SetInt("PlayerState", PlayerState);
    }

    public void SaveButtonInformation(string b1, string b2, string b3, string b4, string b5)
    {
        Button1State = b1;
        PlayerPrefs.SetString("Button1State",Button1State);

        Button2State = b2;
        PlayerPrefs.SetString("Button2State", Button2State);

        Button3State = b3;
        PlayerPrefs.SetString("Button3State", Button3State);

        Button4State = b4;
        PlayerPrefs.SetString("Button4State", Button4State);

        Button5State = b5;
        PlayerPrefs.SetString("Button5State", Button5State);      
    }
    
    public void LoadAllInformation()
    {
        LoadBestScoreGamesPlayedInformation();
        LoadButtonStateInformation();
        LoadCandyCountInformation();
        LoadPlayerStateInformation();
        LoadSoundStateInformation();
    }

    public void DeleteAllInformation()
    {
        PlayerPrefs.DeleteAll();
    }
}
