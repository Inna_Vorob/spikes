﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class StoreScreenController : MonoBehaviour {

    private const int HIDE_SCREEN = -5;
    private const int SHOW_SCREEN = 2;

    private const int NUMBER_OF_BUTTON_1 = 1;
    private const int NUMBER_OF_BUTTON_2 = 2;
    private const int NUMBER_OF_BUTTON_3 = 3;
    private const int NUMBER_OF_BUTTON_4 = 4;
    private const int NUMBER_OF_BUTTON_5 = 5;

    private Canvas _screen;
    private GameObject _buttonOne;
    private GameObject _buttonTwo;
    private GameObject _buttonThree;
    private GameObject _buttonFour;
    private GameObject _buttonFive;

    private Text _textCountOfCandy; 
    private int _countOfCandy;

    private AudioSource _audioButton;
    private AudioClip _audioSelect;
    private AudioClip _audioBack;

    private List<ButtonStoreController> _buttons;
     

    void Awake()
    {
       
        _buttons = new List<ButtonStoreController>();
        _textCountOfCandy = GameObject.Find("CandyCountStore").GetComponent<Text>();
        
        _screen = GameObject.Find("CanvasStore").GetComponent<Canvas>();
        _buttonOne = GameObject.Find("Fish1");
        _buttons.Add(new ButtonStoreController(_buttonOne, "SELECT" ,NUMBER_OF_BUTTON_1));

        _buttonTwo = GameObject.Find("Fish2");
        _buttons.Add(new ButtonStoreController(_buttonTwo, "NOTBUY", NUMBER_OF_BUTTON_2));

        _buttonThree = GameObject.Find("Fish3");
        _buttons.Add(new ButtonStoreController(_buttonThree, "NOTBUY", NUMBER_OF_BUTTON_3));

        _buttonFour = GameObject.Find("Fish4");
        _buttons.Add(new ButtonStoreController(_buttonFour, "NOTBUY", NUMBER_OF_BUTTON_4));

        _buttonFive = GameObject.Find("Fish5");
        _buttons.Add(new ButtonStoreController(_buttonFive, "NOTBUY", NUMBER_OF_BUTTON_5));

        _audioButton = GameObject.Find("CanvasStore").GetComponent<AudioSource>();
        _audioBack = Resources.Load<AudioClip>("Sound/87731__thealmightysound__snap");
        _audioSelect = Resources.Load<AudioClip>("Sound/80921_justinbw_buttonchime02up (mp3cut.net)");

    }   

    public void Click1()
    {
        CheckStateOfButton(NUMBER_OF_BUTTON_1);
        CheckSound(NUMBER_OF_BUTTON_1);
    }

    public void Click2()
    {
        CheckStateOfButton(NUMBER_OF_BUTTON_2);
        CheckSound(NUMBER_OF_BUTTON_2);

    }
    public void Click3()
    {
        CheckStateOfButton(NUMBER_OF_BUTTON_3);
        CheckSound(NUMBER_OF_BUTTON_3);

    }
    public void Click4()
    {
        CheckStateOfButton(NUMBER_OF_BUTTON_4);
        CheckSound(NUMBER_OF_BUTTON_4);

    }
    public void Click5()
    {
        CheckStateOfButton(NUMBER_OF_BUTTON_5);
        CheckSound(NUMBER_OF_BUTTON_5);

    }

    private void CheckStateOfButton(int numberOfButton)
    {
        if (_buttons[numberOfButton - 1].ButtonState == "NOTBUY")
        {
            if ((_countOfCandy - _buttons[numberOfButton - 1].GetHowMuchPlayerCost()) >= 0)
            {
                _countOfCandy = _countOfCandy - _buttons[numberOfButton - 1].GetHowMuchPlayerCost();
                _textCountOfCandy.text = _countOfCandy.ToString();
                _buttons[numberOfButton - 1].CheckState("BUY", numberOfButton);

                for (int i = 0; i < _buttons.Count; i++)
                {
                    if (_buttons[i].ButtonState == "SELECT")
                    {
                        _buttons[i].CheckState("NOTSELECT", i + 1);
                      
                    }
                }                
               
                _buttons[numberOfButton - 1].CheckState("SELECT", numberOfButton);
                GameController.Instance.PlayerController.ChangePlayer(numberOfButton);           
                return;
            }            
        }      

        if (_buttons[numberOfButton - 1].ButtonState == "NOTSELECT")
        {
            for (int i = 0; i < _buttons.Count; i++)
            {
                if(_buttons[i].ButtonState == "SELECT")
                {
                    _buttons[i].CheckState("NOTSELECT", i+1);
                 
                }
            }
            _buttons[numberOfButton - 1].CheckState("SELECT", numberOfButton);
           
            GameController.Instance.PlayerController.ChangePlayer(numberOfButton);       
            return;
        }
    }

    private void PlaySound(AudioClip audio)
    {

        if (PlayerPrefsController.Instance.SoundState == "On")
        {
            if (_audioButton.clip != audio)
            {
                _audioButton.clip = audio;
            }
            _audioButton.Play();
        }
    }

    private void CheckSound(int numberOfButton)
    {
        if (_buttons[numberOfButton - 1].ButtonState == "SELECT" && PlayerPrefsController.Instance.SoundState == "On")
            PlaySound(_audioSelect);
    }

    public void StoreStart()
    {
        _buttons[0].CheckState(PlayerPrefsController.Instance.Button1State, NUMBER_OF_BUTTON_1);
        _buttons[1].CheckState(PlayerPrefsController.Instance.Button2State, NUMBER_OF_BUTTON_2);
        _buttons[2].CheckState(PlayerPrefsController.Instance.Button3State, NUMBER_OF_BUTTON_3);
        _buttons[3].CheckState(PlayerPrefsController.Instance.Button4State, NUMBER_OF_BUTTON_4);
        _buttons[4].CheckState(PlayerPrefsController.Instance.Button5State, NUMBER_OF_BUTTON_5);

        _countOfCandy = PlayerPrefsController.Instance.CandyCount;

        _screen.sortingOrder = SHOW_SCREEN;
        _textCountOfCandy.text = PlayerPrefsController.Instance.CandyCount.ToString();
    }

    public void StoreEnd()
    {
        _screen.sortingOrder = HIDE_SCREEN;        
        _textCountOfCandy.text = "";
        PlayerPrefsController.Instance.SaveButtonInformation(_buttons[0].ButtonState, _buttons[1].ButtonState, _buttons[2].ButtonState, _buttons[3].ButtonState, _buttons[4].ButtonState);
        PlayerPrefsController.Instance.SaveCandyCountInformation(_countOfCandy);      
    }

    public void Back()
    {
        PlaySound(_audioBack);
        GameController.Instance.SetGameState(STATE.START_SCREEN);
    }

    
}
