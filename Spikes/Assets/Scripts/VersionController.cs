﻿using UnityEngine;
using System.Collections;

public static class VersionController {

    private const int _standartWidth = 1536;
    private const int _standartHeight = 2424;   
       
    public static float ScaleOfObject { get { return (_standartHeight * CameraController.WidthCamera)/(_standartWidth * CameraController.HeightCamera); } }


}
