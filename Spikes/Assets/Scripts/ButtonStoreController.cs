﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonStoreController  {

    private Sprite _backGroundImageSprite;
    private Sprite _iconSprite;
    private Sprite _noCandySprite;
     
    public string ButtonState{ get; set; }
    public int ButtonNumber { get; private set; }

    private GameObject _button;   
    private Image [] _buttonIcon;
    private Text _buttonText;

    private Color32 _colorOfText;

    public ButtonStoreController(GameObject button, string state, int number)
    {
        ButtonState = state;       
        ButtonNumber = number;

        _colorOfText = new Color32(63, 182, 120, 255);

        _button = button;        
        _buttonIcon = _button.GetComponentsInChildren<Image>();
        _buttonText = _button.GetComponentInChildren<Text>();
        CheckState(state,number);

    }

    public void CheckState(string state, int number)
    {
        switch (state)
        {
            case "NOTBUY":
                break;

            case "BUY":
                Buy(number);
                break;

            case "SELECT":
                {
                    Buy(number);
                    Select();
                }               
                break;

            case "NOTSELECT":
                {
                    Buy(number);
                    NotSelect();
                }
                break;
        }

    }
     

    private void Buy(int number)
    {
        ButtonState = "BUY";
        _backGroundImageSprite = Resources.Load<Sprite>("Sprites/Button_4");
        _buttonIcon[0].sprite = _backGroundImageSprite;

        _iconSprite = Resources.Load<Sprite>("Sprites/"+number+".1");
        _buttonIcon[1].sprite = _iconSprite;

        _noCandySprite = Resources.Load<Sprite>("Sprites/Box");
        _buttonIcon[2].sprite = _noCandySprite;
        _buttonText.text = "";
        _buttonText.color = _colorOfText;
    }

    private void Select()
    {
        ButtonState = "SELECT";
        _buttonText.text = "";
    }

    private void NotSelect()
    {
        ButtonState = "NOTSELECT";
        _buttonText.text = "SELECT";       
    }

    public int GetHowMuchPlayerCost()
    {
        if (ButtonState == "NOTBUY")
        {
            return int.Parse(_buttonText.text);
        }
        else
            return 0;
    }    
}
