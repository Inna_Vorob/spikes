﻿using UnityEngine;
using System.Collections;

public class PlayerCollisionDetect : MonoBehaviour {

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Contains("Enemy"))
        {
            GameController.Instance.PlayerController.CollisionWithSpikes();
            GameController.Instance.PlayerController.HidePlayer();
        }

        if (collision.gameObject.tag.Contains("Candy"))
        {
            GameController.Instance.PlayerController.CollisionWithCandy(collision.gameObject);
        }

        if (collision.gameObject.tag.Contains("LeftRightWall"))
        {
            GameController.Instance.PlayerController.CollisionWithWalls();
        }
    }
}
