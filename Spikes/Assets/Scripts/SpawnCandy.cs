﻿using UnityEngine;
using System.Collections.Generic;

public class SpawnCandy : MonoBehaviour {

    private float LeftWall { get { return GameController.Instance.UIController.LeftWall + SpawnEnemy.Instance.StepX * 2 + Step; } }
    private float RightWall { get { return GameController.Instance.UIController.RightWall - SpawnEnemy.Instance.StepX * 2 - Step; } }
    private float TopWall { get { return GameController.Instance.UIController.TopWall - SpawnEnemy.Instance.StepX * 2 - Step; } }
    private float BottomWall { get { return GameController.Instance.UIController.BottomWall + SpawnEnemy.Instance.StepX * 2 + Step; } }

    private CandyController _candyOne;

    private GameObject _candy;
    public GameObject Candy
    {
        get
        {
            if (_candy == null)
            {
                _candy = Resources.Load<GameObject>("Prefabs/Candy");
                _candy.transform.localScale = new Vector2(VersionController.ScaleOfObject, VersionController.ScaleOfObject);

                if (_candy == null)
                    Debug.LogError("Can not load Prefabs/Candy");
            }
            return _candy;
        }
    }

    private SpriteRenderer _candySprite;
    public SpriteRenderer CandySprite
    {
        get
        {
            if (_candySprite == null)
                _candySprite = Candy.GetComponent<SpriteRenderer>();

            return _candySprite;
        }
    }

    public bool IsNeed = true;

    private float Step { get { return CandySprite.bounds.size.x / 2f; } }

    public void AddCandy()
    {      

        if (IsNeed && _candyOne!= null)
        {
            if (!_candyOne.IsActive())
            {
                _candyOne.CandyTransform.position = RandomPosition();
                _candyOne.SetActiveYesNo(true);
                
                IsNeed = false;                
            }
        }


        if (_candyOne == null && IsNeed)
        {
            GameObject objectCandy = Instantiate(Candy, RandomPosition(), Candy.transform.rotation) as GameObject;
            _candyOne = new CandyController(objectCandy);           
            IsNeed = false;
        }
    }

    private Vector3 RandomPosition()
    {
        float x = Random.Range(LeftWall, RightWall);
        float y = Random.Range(BottomWall, TopWall);

        return new Vector3(x, y, 0);
    }

    public void DisactiveCandy()
    {
        if(_candyOne != null)
        {
            _candyOne.SetActiveYesNo(false);
            
            IsNeed = true;
        }      
    }  

}
