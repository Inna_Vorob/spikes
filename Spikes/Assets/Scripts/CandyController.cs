﻿using System.Collections;
using UnityEngine;

public class CandyController
{
    public Transform CandyTransform { get; private set; }    
    private GameObject _candy;   
      
    public CandyController(GameObject candy)
    {
        _candy = candy;
        CandyTransform = _candy.GetComponent<Transform>();      
    }

    public bool IsActive()
    {
        return _candy.activeInHierarchy;
    }

    public void SetActiveYesNo(bool set)
    {
        _candy.SetActive(set);
    }    
  
}
