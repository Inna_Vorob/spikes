﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour
{
    private const int HIDE_SCREEN = -5;
    private const int SHOW_SCREEN = 2;

    private const float JUMP_FORCE = 3f;
    private const float SPEED_MOVE = 0.8f;
    private const float GRAVITY_SCALE = -3f;
    private const float TIME_STEP = 2.5f;
    private const float BOUNCE_FORCE = 1f;    

    private ScoreController _score;
    private CandyCountController _countOfCandy;

    private AudioSource _audioPlayer;
    private AudioSource _audioSoundCollect;

    private AudioClip _audioDead;
    private AudioClip _audioCollision;

    private float _jumpForce;
    private float _direction;

    public bool IsAlive { get; private set; }
    
    private PlayerOne _player;
    private GameObject _playerInstance;

    private List<PlayerOne> _poolPlayerssList;
    public List<PlayerOne> PoolPlayerssList
    {
        get
        {
            if (_poolPlayerssList == null)
                _poolPlayerssList = new List<PlayerOne>();

            return _poolPlayerssList;
        }
    }

    void OnEnable()
    {        
        TouchController.OnClick += Jump;        
    }

    void OnDisable()
    {
        TouchController.OnClick -= Jump;        
    }
        
    void Awake()
    {
        _audioPlayer = GameObject.Find("Player").GetComponent<AudioSource>();
        _audioSoundCollect = GameObject.Find("SoundCollect").GetComponent<AudioSource>();
        _audioDead = Resources.Load<AudioClip>("Sound/144015_satanicupsman_bone-flesh-crush-punch (mp3cut.net)");
        _audioCollision = Resources.Load<AudioClip>("Sound/186669_fordps3_computer-boop (mp3cut.net)");     
        ChangePlayer(PlayerPrefsController.Instance.PlayerState);
    }

    void FixedUpdate()
    {       
        if (IsAlive)
        {
            _player.playerRigidbody.velocity = new Vector2(_direction, _jumpForce);
            _jumpForce = Mathf.Lerp(_jumpForce, GRAVITY_SCALE, Time.deltaTime * TIME_STEP);            
        }        
    }

    public void ChangePlayer(int number)
    {
        bool isAddPlayer = false;

        for (int i = 0; i < PoolPlayerssList.Count; i++)
        {           
            if (PoolPlayerssList[i].IsActive())
            {                
                PoolPlayerssList[i].SetActiveYesNo(false);              
            }
        }

        for (int i = 0; i < PoolPlayerssList.Count; i++)
        {          
            if (!PoolPlayerssList[i].IsActive() && PoolPlayerssList[i].numberOfPlayer == number)
            {
                _player = PoolPlayerssList[i];
                PoolPlayerssList[i].SetActiveYesNo(true);
                PlayerPrefsController.Instance.SavePlayerStateInformation(PoolPlayerssList[i].numberOfPlayer);               
                isAddPlayer = true;
                break;               
            }                  
        }

        if (!isAddPlayer)
        {
            _playerInstance = Resources.Load<GameObject>("Prefabs/Player" + number.ToString());
            _playerInstance.transform.localScale =new Vector2( VersionController.ScaleOfObject, VersionController.ScaleOfObject);
            _player = new PlayerOne(Instantiate(_playerInstance, _playerInstance.transform.position, Quaternion.identity) as GameObject, number);
            _poolPlayerssList.Add(_player);          
            PlayerPrefsController.Instance.SavePlayerStateInformation(_player.numberOfPlayer);          
        }
         _player.playerSprite.sortingOrder = HIDE_SCREEN;
        _player.playerRigidbody.isKinematic = true;

    } 

    public void CollisionWithCandy(GameObject candy)
    {
        if (PlayerPrefsController.Instance.SoundState == "On")
        {
            _audioSoundCollect.Play();
        }
        _countOfCandy.ChangeCount();
        candy.SetActive(false);
        GameController.Instance.SpawnCandy.IsNeed = true;
    }

    public void CollisionWithWalls()
    {
        if (IsAlive)
        {
            PlaySound(_audioCollision);
            _direction *= -1;
            _score.ChangeScore();
            GameController.Instance.SpawnCandy.AddCandy();
            _player.playerSprite.flipX = _direction < 0 ? true : false;
            _jumpForce = 2;
        }
    }

    public void CollisionWithSpikes()
    {
        PlaySound(_audioDead);
    }

    private void PlaySound(AudioClip audio)
    {

        if (PlayerPrefsController.Instance.SoundState == "On")
        {
            if (_audioPlayer.clip != audio)
            {
                _audioPlayer.clip = audio;
            }
            _audioPlayer.Play();
        }
    }

    private void Jump()
    {
        _jumpForce = JUMP_FORCE;
        _player.playerParticleSystem.Play();           
    } 
 
    private IEnumerator Dead()
    {
      
        for (float f = 1f; f >= 0; f -= 0.04f)
        {
            _player.PlayerTransform.position += new Vector3(0,-0.01f,0);

            Color c = _player.playerSprite.color;
            c.a = f;
            _player.playerSprite.color = c;
            yield return new WaitForFixedUpdate();
        }
        GameController.Instance.SetGameState(STATE.GAME_OVER_SCREEN);     
        _player.SetActiveYesNo(false);
        yield break;     
    }
   
    public int GetScoreOfThisPlayer()
    {
        return _score.Score;
    }

    public int GetCountCandyOfThisPlayer()
    {
        return _countOfCandy.Count;
    }

    public void HidePlayer()
    {
        IsAlive = false;       
        _player.playerRigidbody.isKinematic = true;       
        _player.playerSprite.color = Color.black;
        StartCoroutine(Dead());
        _jumpForce = 0;    
    }

    public void HidePlayerStart()
    {
       
        _player.playerSprite.sortingOrder = HIDE_SCREEN;
        _player.playerRigidbody.isKinematic = true;  
    }

    public void ShowPlayer()
    {      
        _jumpForce = JUMP_FORCE;
        _player.playerSprite.sortingOrder = SHOW_SCREEN;
        _player.playerRigidbody.isKinematic = false;
        _player.PlayerTransform.position = _player.startPosition;       
        _player.playerSprite.color = _player.startColor;
       
        _player.SetActiveYesNo(true);      
        IsAlive = true;
        _score = new ScoreController(0);
        _countOfCandy = new CandyCountController(PlayerPrefsController.Instance.CandyCount);    
        _direction = SPEED_MOVE;
        _player.playerSprite.flipX = false;     
    }

    public void StartPlayer()
    {
        _player.SetActiveYesNo(true);
        _player.playerSprite.sortingOrder = SHOW_SCREEN;       
       
        _player.playerRigidbody.isKinematic = true;
        _player.playerSprite.color = _player.startColor;
        _player.PlayerTransform.position = _player.startPosition;
        _player.playerSprite.flipX = false;
       
    }
}
