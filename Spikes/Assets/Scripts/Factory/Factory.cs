﻿using UnityEngine;
using System.Collections;

public class Factory
{
    public StateGame GetState(STATE state)
    {
        StateGame st;
        switch (state)
        {
            case STATE.START_SCREEN:
                st = new StartScreen();
                break;

            case STATE.GAME_SCREEN:
                st = new GameScreen();
                break;

            case STATE.GAME_OVER_SCREEN:
                st = new GameOverScreen();
                break;            

            case STATE.MAGAZINE_SCREEN:
                st = new MagazineScreen();
                break;

            case STATE.LOAD_SCREEN:
                st = new LoadScreen();
                break;

            default:
                st = null;
                break;
        }
        return st;
    }
}
