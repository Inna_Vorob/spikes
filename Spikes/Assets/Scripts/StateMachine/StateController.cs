﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StateController 
{   
    private STATE _currentState = STATE.LOAD_SCREEN;
    private STATE _previousState;

    private Dictionary<STATE, StateGame> _states = new Dictionary<STATE, StateGame>();
    private Factory _factory = new Factory();


    public StateController()
    {
        _states.Add(STATE.START_SCREEN, _factory.GetState(STATE.START_SCREEN));
        _states.Add(STATE.GAME_SCREEN, _factory.GetState(STATE.GAME_SCREEN));
        _states.Add(STATE.GAME_OVER_SCREEN, _factory.GetState(STATE.GAME_OVER_SCREEN));
        _states.Add(STATE.MAGAZINE_SCREEN, _factory.GetState(STATE.MAGAZINE_SCREEN));
        _states.Add(STATE.LOAD_SCREEN, _factory.GetState(STATE.LOAD_SCREEN));

        _states[_currentState].OnEnter();
    }

    public void MoveNext(STATE state)
    {
        _previousState = _currentState;
        _currentState = state;

        _states[_previousState].OnExit();
        _states[_currentState].OnEnter();
    }    
}