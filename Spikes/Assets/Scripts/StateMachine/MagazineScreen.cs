﻿using UnityEngine;
using System.Collections;
using System;

public class MagazineScreen :StateGame
{
    public MagazineScreen()
    {
       
    }

    public override void OnEnter()
    {
        GameController.Instance.PlayerController.HidePlayerStart();
        PlayerPrefsController.Instance.LoadButtonStateInformation();        
        GameController.Instance.StoreScreen.StoreStart();
    }

    public override void OnExit()
    {
        GameController.Instance.PlayerController.StartPlayer();
        GameController.Instance.StoreScreen.StoreEnd();
    }    
}
