﻿using UnityEngine;
using System.Collections;
using System;

public class GameOverScreen : StateGame
{
    public GameOverScreen()
    {

    }

    public override void OnEnter()
    {
        GameController.Instance.UIController.SetStartColor();
        GameController.Instance.GameOverScreen.GameOverStart();
        GameController.Instance.SpawnCandy.DisactiveCandy();
    }

    public override void OnExit()
    {
        GameController.Instance.GameOverScreen.GameOverEnd();
        GameController.Instance.UIController.HideLeftRightSpikes();
       
    }


}
