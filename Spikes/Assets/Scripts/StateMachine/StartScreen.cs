﻿using UnityEngine;
using System.Collections;
using System;

public class StartScreen : StateGame
{
    public StartScreen()
    {

    }

    public override void OnEnter()
    {
        PlayerPrefsController.Instance.LoadBestScoreGamesPlayedInformation();
        PlayerPrefsController.Instance.LoadCandyCountInformation();      
        GameController.Instance.PlayerController.StartPlayer();
        GameController.Instance.StartScreen.StartScreenStart();
    }

    public override void OnExit()
    {
        GameController.Instance.StartScreen.StartScreenEnd();
    }

}
