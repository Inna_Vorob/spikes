﻿using UnityEngine;
using System.Collections;
using System;

public class GameScreen : StateGame
{
    public GameScreen()
    {

    }

    public override void OnEnter()
    {      
        GameController.Instance.UIController.ShowScorePanel();
        GameController.Instance.PlayerController.ShowPlayer();
    }

    public override void OnExit()
    {
        GameController.Instance.UIController.HideScorePanel();
        PlayerPrefsController.Instance.SaveBestScoreGamesPlayedInformation(
            GameController.Instance.PlayerController.GetScoreOfThisPlayer());
        PlayerPrefsController.Instance.SaveCandyCountInformation(GameController.Instance.PlayerController.GetCountCandyOfThisPlayer());
    }
}
