﻿using UnityEngine;
using System.Collections;

public enum STATE 
{
    START_SCREEN,
    GAME_SCREEN,
    GAME_OVER_SCREEN,
    MAGAZINE_SCREEN,
    LOAD_SCREEN
};

public abstract class StateGame {

    public abstract void OnEnter();
    public abstract void OnExit();
}
